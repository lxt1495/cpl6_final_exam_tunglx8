package com.example.cpl6_final_exam_tunglx8

/* Problem 3: Implement a queue data structure */
abstract class Queue<T> {
    abstract val size: Int
    abstract fun enqueue(item: T)
    abstract fun dequeue(): T?
    abstract fun peek(): T?
    abstract fun isEmpty(): Boolean
}