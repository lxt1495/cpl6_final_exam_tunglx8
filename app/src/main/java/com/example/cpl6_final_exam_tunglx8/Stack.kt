package com.example.cpl6_final_exam_tunglx8

/* Problem 2: Implement a stack data structure */
abstract class Stack<T> {
    abstract val size: Int
    abstract fun push(item: T)
    abstract fun pop(): T?
    abstract fun peek(): T?
    abstract fun isEmpty(): Boolean
}