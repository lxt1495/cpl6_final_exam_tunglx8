package com.example.cpl6_final_exam_tunglx8

fun main() {

    /* Problem 1 */
//    println(
//        """
//    ${palindrome("abba")}
//    ${palindrome("abcdefg")}
//    ${palindrome(" abba ")}
//    ${palindrome("abb a")}
//    """.trimIndent()
//    )

    /* Problem 2 */
//    var s: Stack<Int>
////    s = StackList<Int>()
//    s = StackLinkedList<Int>()
//    s.add(1)
//    s.add(2)
//    print("Element in stack: ")
//    s.forEach{print("$it ")}
//    println(
//        """
//
//       Stack Size: ${s.size}
//       Stack is empty: ${s.isEmpty()}
//       ${s.remove()}
//       ${s.remove()}
//       ${s.remove()}
//       Stack Size: ${s.size}
//       Stack is empty: ${s.isEmpty()}
//   """.trimIndent()
//    )

    /* Problem 3 */
//    var q1: Queue<Int>  // Test 1
//    q1 = QueueList<Int>()
////    q1 = QueueLinkedList<Int>()
////    q1 = QueueStackList<Int>()
////    q1 = QueueStackLinkedList<Int>()
//    q1.enqueue(1)
//    println(
//        """
//        ${q1.dequeue()}
//        ${q1.dequeue()}
//    """.trimIndent()
//    )

//    var q2: Queue<Char>  // Test 2
//    q2 = QueueList<Char>()
////    q2 = QueueLinkedList<Char>()
////    q2 = QueueStackList<Char>()
////    q2 = QueueStackLinkedList<Char>()
//    println("Initial queue is empty: ${q2.isEmpty()}")
//    q2.enqueue('A')
//    println("Queue is empty after add: ${q2.isEmpty()}")
//    q2.enqueue('B')
//    q2.enqueue('C')
//    print("Element in queue: ")
//    q2.forEach { print("$it ") }
//    println(
//        """
//
//       Remove ${q2.dequeue()}
//       First element: ${q2.peek()}
//       First element: ${q2.peek()}
//       Remove ${q2.dequeue()}
//       Remove ${q2.dequeue()}
//       Remove ${q2.dequeue()}
//    """.trimIndent()
//    )

}

/* Problem 1: isPalindrome */

fun palindrome(input: String) = input.trim() == input.trim().reversed()