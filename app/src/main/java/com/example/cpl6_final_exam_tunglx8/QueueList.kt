package com.example.cpl6_final_exam_tunglx8

import java.util.function.Consumer

/* Problem 3: Implement a queue data structure */
class QueueList<T> : Queue<T>() {
    private var list = emptyList<T>()
    override val size: Int
        get() = list.size

    override fun isEmpty() = list.isEmpty()

    override fun enqueue(item: T) {
        list = list + item
    }

    override fun dequeue(): T? {
        if (!isEmpty()) {
            val first = list.first()
            list = if (size == 1) emptyList() else list.subList(1, size)
            return first
        }
        return null
    }

    override fun peek() = if (!isEmpty()) list.first() else null

    fun forEach(consumer: Consumer<T>) {
        list.forEach(consumer)
    }

}