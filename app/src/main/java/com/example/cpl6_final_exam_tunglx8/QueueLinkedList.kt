package com.example.cpl6_final_exam_tunglx8

import java.util.*
import java.util.function.Consumer

/* Problem 3: Implement a queue data structure */
class QueueLinkedList<T> : Queue<T>() {
    private val list = LinkedList<T>()
    override val size: Int
        get() = list.size

    override fun isEmpty() = list.isEmpty()

    override fun enqueue(item: T) {
        list.add(item)
    }

    override fun dequeue() = if (!isEmpty()) list.removeFirst() else null

    override fun peek() = if (!isEmpty()) list.first else null

    fun forEach(consumer: Consumer<T>) {
        list.forEach(consumer)
    }

}