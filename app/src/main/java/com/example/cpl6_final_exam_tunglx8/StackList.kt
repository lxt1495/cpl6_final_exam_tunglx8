package com.example.cpl6_final_exam_tunglx8

import java.util.function.Consumer

/* Problem 2: Implement a stack data structure */
class StackList<T> : Stack<T>() {
    private var list = emptyList<T>()
    override val size: Int
        get() = list.size

    override fun isEmpty() = list.isEmpty()

    override fun push(item: T) {
        list = list + item
    }

    fun add(item: T) {
        list = list + item
    }

    override fun pop(): T? {
        if (!isEmpty()) {
            val last = list.last()
            list = if (size == 1) emptyList() else list.subList(0, size - 1)
            return last
        }
        return null
    }

    fun remove(): T? {
        if (!isEmpty()) {
            val last = list.last()
            list = if (size == 1) emptyList() else list.subList(0, size - 1)
            return last
        }
        return null
    }

    override fun peek() = if (!isEmpty()) list.last() else null

    fun forEach(consumer: Consumer<T>) {
        list.forEach(consumer)
    }

    fun first() = if (!isEmpty()) list.first() else null

    fun removeFirst(): T? {
        val first = first()
        if (!isEmpty())
            list = if (size == 1) emptyList() else list.subList(1, size)
        return first
    }

}