package com.example.cpl6_final_exam_tunglx8

import java.util.*
import java.util.function.Consumer

/* Problem 2: Implement a stack data structure */
class StackLinkedList<T> : Stack<T>() {
    private val list = LinkedList<T>()
    override val size: Int
        get() = list.size

    override fun isEmpty() = list.isEmpty()

    override fun push(item: T) {
        list.add(item)
    }

    fun add(item: T) {
        list.add(item)
    }

    override fun pop() = if (!isEmpty()) list.removeLast() else null

    fun remove() = if (!isEmpty()) list.removeLast() else null

    override fun peek() = if (!isEmpty()) list.last else null

    fun forEach(consumer: Consumer<T>) {
        list.forEach(consumer)
    }

    fun removeFirst() = if (!isEmpty()) list.removeFirst() else null

    fun first() = if (!isEmpty()) list.first else null

}