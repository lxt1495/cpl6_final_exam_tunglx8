package com.example.cpl6_final_exam_tunglx8

import java.util.function.Consumer

/* Problem 3: Implement a queue data structure */
class QueueStackLinkedList<T> : Queue<T>() {

    private val stack = StackLinkedList<T>()

    override val size: Int
        get() = stack.size

    override fun isEmpty() = stack.isEmpty()

    override fun enqueue(item: T) {
        stack.add(item)
    }

    override fun dequeue() = stack.removeFirst()

    override fun peek() = stack.first()

    fun forEach(consumer: Consumer<T>) {
        stack.forEach(consumer)
    }
}